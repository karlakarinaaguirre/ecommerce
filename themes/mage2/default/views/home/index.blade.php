@extends('layouts.app')

@section('meta_title','Home Page')
@section('meta_description','Home Page')


@section('content')
<!-- CABALLEROU AREA END -->
<!-- SLIDER AREA START -->
<section class="slider-area-main">
  <!-- slider -->
  <div class="slider-area">
    <div class="bend niceties preview-1">
      <div id="ensign-nivoslider-3" class="slides">
        <img src="{{asset('vendor/mage2-default/img/home-1/slider-1.jpg')}}" alt="" title="#slider-direction-1"  />
        <img src="{{asset('vendor/mage2-default/img/home-1/slider-2.jpg.png')}}" alt="" title="#slider-direction-2"  />
        <img src="{{asset('vendor/mage2-default/img/home-1/slider-3.jpg')}}" alt="" title="#slider-direction-3"  />
      </div>
      <!-- direction 1 -->
      <div id="slider-direction-1" class="t-cn slider-direction">
        <div class="slider-content t-lfl s-tb slider-1">
          <div class="title-container s-tb-c">
            <h1 class="title1">La mejor colección</h1>
            <p class="title1">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
          </div>
        </div>
      </div>
      <!-- direction 2 -->
      <div id="slider-direction-2" class="slider-direction">
        <div class="slider-content t-cn s-tb slider-2">
          <div class="title-container s-tb-c">
            <h1 class="title1">Estilos sorprendentes</h1>
            <h3 class="title3">Para la moda</h3>
          </div>
        </div>
      </div>
      <!-- direction 3 -->
      <div id="slider-direction-3" class="slider-direction">
        <div class="slider-content t-lfr s-tb slider-3">
          <div class="title-container s-tb-c">
            <h1 class="title1">Nuestra última colección</h1>
            <h3 class="title3">Calzado de moda</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- slider end-->
</section>
<!-- SLIDER AREA END -->
<!-- BANNER AREA START -->
<section class="banner-area">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 xs-res-mrbtm">
        <div class="banner-left">
          <a class="promo-link" href="{{route('category.view',['slug'=>'caballero'])}}">
            <img src="{{asset('vendor/mage2-default/img/home-1/banner-1.jpg')}}" alt="" />
            <h1>Colección para caballero</h1>
            <span class="promo-hover"></span>
          </a>
        </div>
      </div>
      <div class="col-md-3 col-sm-3">
        <div class="banner-left-side">
          <a class="mr-btm promo-link" href="{{route('category.view',['slug'=>'nueva-coleccion-caballero'])}}">
            <img src="{{asset('vendor/mage2-default/img/home-1/banner-2.jpg')}}" alt="" />
            <h1>Nueva colección para caballero</h1>
            <span class="sl-btn">VENTA</span>
            <div class="promo-hover"></div>
          </a>
          <a class="promo-link xs-res-mrbtm"href="{{route('category.view',['slug'=>'mejor-coleccion-caballero'])}}">
            <img src="{{asset('vendor/mage2-default/img/home-1/banner-3.jpg')}}" alt="" />
            <h1>La mejor colección para caballero</h1>
            <span class="sl-btn">VENTA</span>
            <div class="promo-hover"></div>
          </a>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 xs-res-mrbtm">
        <div class="banner-right">
          <a class="promo-link" href="{{route('category.view',['slug'=>'dama'])}}">
            <img src="{{asset('vendor/mage2-default/img/home-1/banner-4.jpg')}}" alt="" />
            <h1>Colección para dama</h1>
            <span class="promo-hover"></span>
          </a>
        </div>
      </div>
      <div class="col-md-3 col-sm-3">
        <div class="banner-right-side">
          <a class="mr-btm promo-link" href="{{route('category.view',['slug'=>'nueva-coleccion-dama'])}}">
            <img src="{{asset('vendor/mage2-default/img/home-1/banner-5.jpg')}}" alt="" />
            <h1>Nueva colección para dama</h1>
            <span class="sl-btn">VENTA</span>
            <div class="promo-hover"></div>
          </a>
          <a class="promo-link"  href="{{route('category.view',['slug'=>'mejor-coleccion-dama'])}}">
            <img src="{{asset('vendor/mage2-default/img/home-1/banner-6.jpg')}}" alt="" />
            <h1>La mejor colección para dama</h1>
            <span class="sl-btn">VENTA</span>
            <div class="promo-hover"></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- BANNER AREA END -->
<!-- NEW ARRIVALS START -->
<section class="featured-area new-arrival">
  <div class="container">
    <div class="row">
      <div class="text-center">
        <div class="section-titel">
          <h3>NUEVA MERCANCIA</h3>
        </div>
      </div>
      <div class="newarrival-area">
        <div id="newarrival-curosel" class="indicator-style">
          @foreach($nuevaMercancia as $product)
              @include('catalog.product.view.product-card-new',['product' => $product])
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

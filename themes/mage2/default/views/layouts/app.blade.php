<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'LacaLaca') }}</title>

    <!-- Styles -->
     {{-- <link href="{{ asset('vendor/mage2-default/css/app.css') }}" rel="stylesheet">  --}}


    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('vendor/mage2-default/img/favicon.ico')}}">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- all css here -->
    <!-- bootstrap v3.3.6 css -->
    <link rel="stylesheet" href="{{asset('vendor/mage2-default/css/bootstrap.min.css')}}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{asset('vendor/mage2-default/css/animate.css')}}">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="{{asset('vendor/mage2-default/lib/css/nivo-slider.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('vendor/mage2-default/lib/css/preview.css')}}" type="text/css" media="screen" />
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="{{asset('vendor/mage2-default/css/jquery-ui.min.css')}}">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{asset('vendor/mage2-default/css/meanmenu.min.css')}}">
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="{{asset('vendor/mage2-default/css/owl.carousel.css')}}">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="{{asset('vendor/mage2-default/css/font-awesome.min.css')}}">
    <!-- style css -->
    <link rel="stylesheet" href="{{asset('vendor/mage2-default/style.css')}}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{asset('vendor/mage2-default/css/responsive.css')}}">
    <!-- modernizr css -->
    <script src="{{asset('vendor/mage2-default/js/modernizr-2.8.3.min.js')}}"></script>
</head>

<body>
    <div id="app">
        <div class="menu-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <div class="logo-area">
                            <a href="index.html"><img src="{{asset('vendor/mage2-default/img/FondoNegro.png')}}" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <div class="main-menu">
                            <ul class="list-inline">
                                <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{route('home')}}">INICIO</a></li>
                                <li class="{{ Request::is('category/dama') ? 'active' : '' }}"><a href="{{route('category.view',['slug'=>'dama'])}}">DAMA</a></li>
                                <li class="{{ Request::is('category/caballero') ? 'active' : '' }}"><a href="{{route('category.view',['slug'=>'caballero'])}}">CABALLERO</a></li>
                                <li class="{{ Request::is('category/catalogo') ? 'active' : '' }}"><a href="{{route('category.view',['slug'=>'catalogo'])}}">CATALOGO</a></li>                                
                                <li class="{{ Request::is('contact') ? 'active' : '' }}"><a href="{{route('contact')}}">CONTACTANOS</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- MOBILE-MENU-AREA START -->
                    <div class="mobile-menu-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mobile-area">
                                        <div class="mobile-menu">
                                            <nav id="mobile-nav" style="display: block;">
                                                <ul>
                                                    <li><a href="{{route('home')}}">INICIO</a></li>
                                                    <li><a href="{{route('category.view',['slug'=>'dama'])}}">DAMA</a></li>
                                                    <li><a href="{{route('category.view',['slug'=>'caballero'])}}">CABALLERO</a></li>
                                                    <li><a href="{{route('contact')}}">CONTACTANOS</a></li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MOBILE-MENU-AREA END  -->
                    <div class="col-md-2 col-sm-2">
                        <div class="menu-right-area">
                            <ul>
                                <li>
                                    <a href="#" class="cart-toggler search-icon"><i class="fa fa-search"></i></a>
                                    <div class="header-bottom-search" style="display: none;">
                                        <form method="GET" action="{{route('search.result') }}" class="search-box">
                                            <div>
                                                <input type="text" autocomplete="off" placeholder="Buscar" name="q" value="">
                                            </div>
                                        </form>
                                    </div>
                                </li>
                                <li>
                                    <?php
                                    $itemsCart = 0;
                                    $cartItems = (Session::get('cart') != null) ? Session::get('cart') : [];
                                    foreach ($cartItems as $c):
                                      $itemsCart = $itemsCart + $c['qty'];
                                    endforeach; ?>
                                    <a href="javascript:void()" class="cart-toggler mini-cart-icon"><i class="fa fa-shopping-cart"></i>@if($itemsCart>0)<span>{{$itemsCart}}</span>@endif</a>
                                    <div class="top-cart-content">

                                        @foreach($cartItems as $product)
                                        <div class="media header-middle-checkout">
                                            <div class="media-left check-img">
                                              <a href="{{ route('product.view', $product['slug'])}}" title="{{ $product['name'] }}">
                                                @if(file_exists( public_path(). $product['relativePath']))
                                                <img class="img-responsive" src="{{ $product['image'] }}" alt="{{ $product['name'] }}" style="width:80px">
                                                @else
                                                <img class="img-responsive" src="{{ asset('img/enlace-roto.png') }}" alt="{{ $product['name'] }}" style="width:80px">
                                                @endif
                                              </a>
                                            </div>
                                            <div class="media-body checkout-content">
                                                <h4 class="media-heading">
                                                    <a href="{{ route('product.view', $product['slug'])}}">{{$product['name']}}</a>

                                                    <a title="Quitar esta compra" class="btn-remove checkout-remove" href="{{ route('carrito.eliminar', $product['id'])}}"><i class="fa fa-trash"></i></a>
                                                </h4>
                                                <p>{{$product['qty']}} x ${{number_format($product['final_price'],2)}}</p>
                                            </div>
                                        </div>
                                        @endforeach

                                        <div class="actions">
                                            <a type="button" title="Checkout-botton" class="Checkout-botton" style="width:100%" href="{{route('carrito.mostrar')}}"><span>Revisar Carrito</span></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="{{route('admin.login')}}" class="user-login mini-cart-icon"><i class="fa fa-user"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <main class="">
            @yield('content')
        </main>
    </div>

    <!-- Scripts -->
    <script src="{{asset('vendor/mage2-default/js/app.js')}}"></script>

    <!-- all js here -->
    <!-- jquery latest version -->
    <script src="{{asset('vendor/mage2-default/js/jquery-1.12.0.min.js')}}"></script>
    <!-- bootstrap js -->
    <script src="{{asset('vendor/mage2-default/js/popper.min.js')}}"></script>
    
    <script src="{{asset('vendor/mage2-default/js/bootstrap.min.js')}}"></script>
    <!-- owl.carousel js -->
    <script src="{{asset('vendor/mage2-default/js/owl.carousel.min.js')}}"></script>
    <!-- meanmenu js -->
    <script src="{{asset('vendor/mage2-default/js/jquery.meanmenu.js')}}"></script>
    <!-- Nivo slider js  -->
    <script src="{{asset('vendor/mage2-default/lib/js/jquery.nivo.slider.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendor/mage2-default/lib/home.js')}}" type="text/javascript"></script>
    <!-- count down js  -->
    <script src="{{asset('vendor/mage2-default/js/jquery.countdown.js')}}"></script>
    <!-- jquery-ui js -->
    <script src="{{asset('vendor/mage2-default/js/jquery-ui.min.js')}}"></script>
    <!-- wow js -->
    <script src="{{asset('vendor/mage2-default/js/wow.min.js')}}"></script>
    <!-- plugins js -->
    <script src="{{asset('vendor/mage2-default/js/plugins.js')}}"></script>
    <!-- main js -->
    <script src="{{asset('vendor/mage2-default/js/main.js')}}"></script>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    
    @stack('scripts')

</body>

</html>

@extends('layouts.app')

@section('meta_title')
    Resultado de busqueda
@endsection

@section('content')
<section class="bannerhead-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h1>Resultado de busqueda</h1>
						</div>
					</div>
				</div>
			</div>
		</section>

    <section class="wishlist-area shop-area">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="wishlist-left-area">
							<div class="category">
								<h4>CATEGORIAS</h4>
								<div class="category-list">
									<ul>
										<li>
											@include('layouts.category-tree',['categories', $categories])										
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="shop-right-area">
							<div class="shop-tab-area">
								<!--NAV PILL-->
								<div class="shop-tab-pill">
									<ul>
										<li class="active" id="p-mar">
											<a data-toggle="pill" href="#grid">
												<i class="fa fa-th" aria-hidden="true"></i>
												<span>Cuadricula</span>
											</a>
										</li>
										<li>
											<a data-toggle="pill" href="#list">
												<i class="fa fa-th-list" aria-hidden="true"></i>
												<span>Lista</span>
											</a>
										</li>
										<li class="product-size-deatils">
											<div class="show-label">
												<label>Mostrar : </label>
												<select>
													<option value="10" selected="selected">10</option>
													<option value="09">09</option>
													<option value="08">08</option>
													<option value="07">07</option>
													<option value="06">06</option>
												</select>
											</div>
										</li>
									</ul>
								</div>
								<!--NAV PILL-->
								<div class="tab-content">
									<div class="row tab-pane active" id="grid">
                    <!-- PRODUCTO SINGLE -->
                    @foreach($products as $product)
                      @include('catalog.product.view.product-card',['product' => $product])
                    @endforeach
                    <!-- PRODUCTO SINGLE -->
                  </div>
                </div>
								<!--NAV PILL-->
								<div class="shop-tab-pill dwn">
									  {!!  $products->links('pagination.bootstrap-4') !!}
								</div>
								<!--NAV PILL-->
							</div>
						</div>
					</div>
				</div>
			</div>
		 </section>

@endsection

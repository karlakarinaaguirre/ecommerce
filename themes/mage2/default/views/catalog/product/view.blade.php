@extends('layouts.app') @section('meta_title') {{ $product->title }} @endsection @section('content')


<div class="product-simple-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="single-product-image">
					<div class="single-product-tab">

						<!-- Tab panes -->
						<div class="tab-content">

							<div role="tabpanel" class="tab-pane active" id="home">
								@include('catalog.product.view.product-image',['imageType' => 'largeUrl'])
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="single-product-info">
					<div class="product-nav">
						<a href="#">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					<h1 class="product_title">{{ $product->name }}</h1>
					<div class="price-box">
						<span>$ {{number_format($product->price, 2 )}}</span>
					</div>
					<div class="short-description">
						<p>{!! $product->description !!}</p>
					</div>
					@if($product->type == 'BASIC') @include('catalog.product.view.type.basic-add-to-cart') @elseif($product->type == 'VARIATION'
					) @include('catalog.product.view.type.variation-add-to-cart') @endif
				</div>
			</div>
		</div>
	</div>
</div>
<div class="product-tab-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-9">
				<div class="product-tabs">
					<div>
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#tab-desc" aria-controls="tab-desc" role="tab" data-toggle="tab">Descripcion</a>
							</li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="tab-desc">
								<div class="product-tab-desc">
									<p>{!! $product->description !!}</p>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
<div class="col-md-4 col-sm-4">
  <div class="single-product">
    <div class="product-image">
      <a class="product-img" href="{{ route('product.view', $product->slug)}}" title="{{ $product->name }}">
          @include('catalog.product.view.product-image',['product' => $product,'imageType'=>'medUrl'])
      </a>
    </div>
    <span class="onsale">
      @if($product->availableQty()>0)
      <span class="sale-text">{{number_format($product->availableQty(),0)}}</span>
      @endif
    </span>
    
    <div class="product-action">
      <h4><a href="{{ route('product.view', $product->slug)}}" class="product-name"
         title="{{ $product->name }}">
          {{ $product->name }}
      </a></h4>
      <span class="price">
          $ {{ number_format($product->price,2) }}
      </span>
    </div>
    <div class="pro-action">
      <ul>
        <li>
          @if($product->canAddtoCart() && $product->availableQty()>0)
          <form method="post" action="{{ route('carrito.agregar') }}">
              {{ csrf_field() }}
          <input type="hidden" name="slug" value="{{ $product->slug }}"/>
          <div class="clearfix"></div>
          <button type="submit" class="test all_src_icon"
                  href="{{ route('carrito.agregar', $product->id) }}" title="" data-toggle="tooltip" data-placement="top" data-original-title="Agregar compra">
              <i class="fa fa-cart-plus fa-lg" aria-hidden="true"></i>
          </button>
          </form>

          @else
              <div class="product-stock text-white ">Producto agotado</div>
              <hr>
              <div class="clearfix"></div>
          @endif
        </li>
      </ul>
    </div>
  </div>
</div>

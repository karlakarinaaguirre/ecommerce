<?php

$image = $product->image;
$imageType = (isset($imageType)) ? $imageType : "smallUrl"
?>
@if(NULL !== $image)
<a href="{{ route('product.view', $product->slug)}}" title="{{ $product->name }}">
  @if(file_exists( public_path(). $image->relativePath))
  <img class="primary-img img-responsive" src="{{ $image->$imageType }}" alt="{{ $product->title }}">
  @else
  <img class="primary-img img-responsive" src="{{ asset('img/enlace-roto.png') }}" alt="{{ $product->title }}">
  @endif
</a>
 
@endif


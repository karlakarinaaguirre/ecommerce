<?php

$image = $product->image;
$imageType = (isset($imageType)) ? $imageType : "smallUrl"
?>
@if(NULL !== $image)
  @if(file_exists( public_path(). $image->relativePath))
  <img class="primary-img img-responsive" src="{{ $image->$imageType }}" alt="{{ $product->title }}">
  @else
  <img class="primary-img img-responsive" src="{{ asset('img/enlace-roto.png') }}" alt="{{ $product->title }}">
  @endif
  <!-- <img class="secondary-img img-responsive" src="{{ $image->$imageType }}" alt=""> -->
@endif

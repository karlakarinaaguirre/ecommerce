 @if($product->canAddtoCart())

<form method="post" action="{{ route('carrito.agregar') }}">
    {{ csrf_field() }}
    <input type="hidden" name="slug" value="{{ $product->slug }}" />
    <div class="quantity">
        <input type="number" name="qty" class="form-control" value="1">
        <button type="submit" class="btn btn-primary" href="{{ route('carrito.agregar', $product->id) }}">Agregar compra</button>
    </div>
    <div class="clearfix"></div>
</form>


@endif
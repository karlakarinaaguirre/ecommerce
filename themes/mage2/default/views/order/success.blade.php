@extends('layouts.app')

@section('content')
<section class="bannerhead-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h2>Pedido realizado correctamente</h2>
						<embed src="{{url('/uploads/order/invoice/'.$order->id.'.pdf')}}" width="600" height="500" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">

						</div>
					</div>
				</div>
			</div>
		</section>

@endsection

@extends('layouts.app')

@section('meta_title','Home Page')
@section('meta_description','Home Page')


@section('content')
<!-- MENU AREA END -->
<!-- MAP-AREA START -->
<div class="map-area">
  <div id="googleMap" style="width:100%;height:445px;">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3643.9404681579017!2d-104.65625658501328!3d24.03316438445087!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x869bb7d96b9e35e5%3A0xe9402605879a5250!2sProl+G%C3%B3mez+Palacio+972%2C+F%C3%A1tima%2C+34060+Durango%2C+Dgo.!5e0!3m2!1ses-419!2smx!4v1519655163746" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>
<!-- MAP-AREA END -->
<!-- ADDRESS AREA START -->
<section class="adress-area">
  <div class="container">
    <div class="row">
      <div class="col-md-4 res-mr-btm xs-res-mrbtm">
        <div class="address-single">
          <div class="all-adress-info">
            <div class="icon">
              <span><i class="fa fa-user-plus"></i></span>
            </div>
            <div class="info">
              <h3>TELEFONO</h3>
              <p>01 618 160 7380</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 res-mr-btm xs-res-mrbtm">
        <div class="address-single">
          <div class="all-adress-info">
            <div class="icon">
              <span><i class="fa fa-map-marker"></i></span>
            </div>
            <div class="info">
              <h3>DIRECCION</h3>
              <p>Prol Gómez Palacio 972</p>
              <p>Fátima, 34060 Durango, Dgo</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="address-single">
          <div class="all-adress-info">
            <div class="icon">
              <i class="fa fa-envelope"></i>
            </div>
            <div class="info">
              <h3>CORREO</h3>
              <p>...</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- ADDRESS AREA END -->
<!-- CONTACT FROM AREA START -->
<section class="contact-from-area">
  <div class="container">
    <div class="contact-from-heading">
      <h3>DEJANOS UN MENSAJE</h3>
    </div>
    <div class="row">
      <form action="mailto:correo@hotmail.com" method="POST">
        <div class="col-md-6">
          <div class="contact-from-left">
            <div class="input-text">
              <input type="text" placeholder="NOMBRE" name="name" id="name"/>
            </div>
            <div class="input-text">
              <input type="email" placeholder="CORREO" name="email" id="email"/>
            </div>
            <div class="input-text">
              <input type="text" placeholder="ASUNTO" name="subject" id="subject"/>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="contact-from-right">
            <div class="input-message">
              <textarea id="message" placeholder="Your Message" name="message"></textarea>
              <input type="submit" value="SEND" name="submitMessage" id="submitMessage"/>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>

@endsection


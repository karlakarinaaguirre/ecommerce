@extends('layouts.app') @section('content')
<section class="bannerhead-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-heading">
                    <h1>PASO DE COMPROBACION</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<form id="place-order-form" method="post" action="{{ route('order.place') }}">
    {{ csrf_field() }}
    <section class="checkout-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 res-mr-btm">
                    <div class="checkout-left-area">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="">
                                            <span>1</span>Información de facturación</a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="country-select">
                                                <label>PAIS
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="form-group">
                                                    <select name="billing[country_id]" data-name="country_id" @if($errors->has('billing.country_id')) class="is-invalid billing-country form-control
                                                        billing tax-calculation" @else class="billing-country form-control
                                                        billing tax-calculation" @endif > @foreach($countries as $countryId
                                                        => $countryName) @if($countryName =='Mexico')
                                                        <option value="{{ $countryId }}" selected="selected">{{ $countryName }}</option>
                                                        @else
                                                        <option value="{{ $countryId }}">{{ $countryName }}</option>
                                                        @endif @endforeach
                                                    </select>

                                                    @if ($errors->has('billiing.country_id'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('billing.country_id') }}
                                                    </div>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout-form-list">
                                                <label>Nombre
                                                    <span class="required">*</span>
                                                </label>
                                                <input type="text" name="billing[first_name]" id="input-user-first-name" @if($errors->has('billing.first_name')) class="is-invalid form-control" @else class="form-control"
                                                @endif /> @if ($errors->has('billing.first_name'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('billing.first_name') }}
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout-form-list">
                                                <label>Apellido
                                                    <span class="required">*</span>
                                                </label>
                                                <input type="text" name="billing[last_name]" id="input-user-last-name" @if($errors->has('billing.last_name')) class="is-invalid form-control" @else class="form-control"
                                                @endif /> @if ($errors->has('billing.last_name'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('billing.last_name') }}
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Direccion
                                                    <span class="required">*</span>
                                                </label>
                                                <input type="text" placeholder="Calle / Colonia / Numero" name="billing[address1]" value="" id="input-billing-address-1"
                                                    @if($errors->has('billing.address1')) class="is-invalid form-control" @else class="form-control"
                                                @endif /> @if ($errors->has('billing.address1'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('billing.address1') }}
                                                </div>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <input type="text" name="billing[address2]" value="" placeholder="Apartamento, suite, color etc. (opcional)" id="input-billing-address-2"
                                                    @if($errors->has('billing.address2')) class="is-invalid form-control" @else class="form-control"
                                                @endif /> @if ($errors->has('billing.address2'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('billing.address2') }}
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Ciudad / Locacion
                                                    <span class="required">*</span>
                                                </label>
                                                <input type="text" data-name="city" name="billing[city]" placeholder="City" id="input-billing-city" @if($errors->has('billing.city')) class="is-invalid billing tax-calculation form-control"
                                                @else class="billing tax-calculation form-control" @endif /> @if($errors->has('billing.city'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('billing.city') }}
                                                </div>
                                                @endif </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout-form-list">
                                                <label>Region / Estado
                                                    <span class="required">*</span>
                                                </label>
                                                <input type="text" name="billing[state]" data-name="state_code" id="input-billing-zone" @if($errors->has('billing.state')) class="is-invalid billing tax-calculation form-control"
                                                @else class="billing tax-calculation form-control" @endif /> @if($errors->has('billing.state'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('billing.state') }}
                                                </div>
                                                @endif </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout-form-list">
                                                <label>Codigo Postal
                                                    <span class="required">*</span>
                                                </label>
                                                <input type="text" data-name="postcode" name="billing[postcode]" value="" placeholder="Post Code" id="input-billing-postcode"
                                                    @if($errors->has('billing.postcode')) class="is-invalid billing tax-calculation form-control"
                                                @else class="billing tax-calculation form-control" @endif /> @if ($errors->has('billing.postcode'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('billing.postcode') }}
                                                </div>
                                                @endif </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout-form-list">
                                                <label>Correo electronico
                                                    <span class="required">*</span>
                                                </label>
                                                <input type="text" name="user[email]" placeholder="E-Mail" id="input-user-email" @if($errors->has('user.email')) class="is-invalid form-control" @else class="form-control"
                                                @endif > @if ($errors->has('user.email'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('user.email') }}
                                                </div>
                                                @endif </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout-form-list">
                                                <label>Telefono
                                                    <span class="required">*</span>
                                                </label>
                                                <input type="text" name="billing[phone]" placeholder="Phone" id="input-billing-phone" @if($errors->has('billing.phone')) class="is-invalid form-control" @else class="form-control"
                                                @endif /> @if ($errors->has('billing.phone'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('billing.phone') }}
                                                </div>
                                                @endif </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="">
                                            <span>2</span>Revisar pedido</a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="order-review" id="checkout-review">
                                            <div class="table-responsive" id="checkout-review-table-wrapper">
                                                <table class="data-table" id="checkout-review-table">
                                                    <thead>
                                                        <tr>
                                                            <th rowspan="1">Producto</th>
                                                            <th colspan="1">Precio</th>
                                                            <th rowspan="1">Cantida</th>
                                                            <th colspan="1">Subtotal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $subTotal = 0; $totalTax = 0; $giftCouponAmount = 0; @endphp @foreach($cartItems as $cartItem) @php $giftCouponAmount
                                                        += (isset($cartItem['gift_coupon_amount'])) ? $cartItem['gift_coupon_amount']
                                                        : 0.00; @endphp
                                                        <tr>
                                                            <td>
                                                                <h3 class="product-name">{{ $cartItem['name'] }}</h3>
                                                                <?php $attributeText = ''; ?> @if(isset($cartItem['attributes']) && count($cartItem['attributes'])
                                                                > 0) @foreach($cartItem['attributes'] as $attribute) @if($loop->last)
                                                                <?php $attributeText .= $attribute['variation_display_text']; ?> @else
                                                                <?php $attributeText .= $attribute['variation_display_text'].': '; ?> @endif @endforeach


                                                                <p>Caracteristicas:
                                                                    <span class="text-success">
                                                                        <strong>{{ $attributeText}}</strong>
                                                                    </span>
                                                                </p>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <span class="cart-price">
                                                                    <span class="price"> ${{ number_format($cartItem['final_price'],2) }}
                                                                    </span>
                                                                </span>
                                                            </td>
                                                            <td>{{ $cartItem['qty'] }}</td>
                                                            <!-- sub total starts here -->
                                                            <td>
                                                                <span class="cart-price">
                                                                    <span class="price">${{ number_format($cartItem['qty'] * $cartItem['final_price'],
                                                                        2) }}</span>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <?php $totalTax += $cartItem['qty'] * $cartItem['tax_amount']; ?>
                                                        <?php $subTotal += $cartItem['qty'] * $cartItem['final_price']; ?>
                                                        <input type="hidden" name="products[]" value="{{ $cartItem['id'] }}" /> @endforeach

                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="3">Subtotal</td>
                                                            <td class="sub-total" data-sub-total="{{ $subTotal }}">
                                                                <span class="price"> ${{ number_format($subTotal ,2)}}</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">Descuento</td>
                                                            <td class="gif-coupon" data-gif-coupon="{{ $giftCouponAmount }}">
                                                                <span class="price"> ${{ number_format($giftCouponAmount ,2)}}</span>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="3">IVA</td>
                                                            <td class="tax-amount" data-tax-amount="{{ $totalTax }}">
                                                                <span class="price">${{ number_format($totalTax=($subTotal-$giftCouponAmount)*.16,2)
                                                                    }}
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <strong>Total a pagar</strong>
                                                            </td>
                                                            <td class="total" data-total="{{ ($subTotal-$giftCouponAmount)+$totalTax }}">
                                                                <strong>
                                                                    <span class="price">${{ ($subTotal-$giftCouponAmount)+$totalTax }}</span>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div id="checkout-review-submit">
                                                <div class="cart-btn-3" id="review-buttons-container">
                                                    <div class="buttons clearfix">
                                                        <div class="float-right">
                                                            He leido y acepto los
                                                            <a href="{{ $termConditionPageUrl }}" target="_blank" class="agree">
                                                                <b>Terminos &amp; Condiciones
                                                                </b>
                                                            </a>
                                                            <input id="agree" type="checkbox" name="agree" /> &nbsp;
                                                        </div>
                                                    </div>

                                                    <div id="paypal-button-container" class="payment float-right clearfix hidden">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>



@endsection @push('scripts')

<script>
    paypal.Button.render({
        env: "{{$pay['env']}}",
        style: {
            label: 'paypal',
            size: 'medium', // small | medium | large | responsive
            shape: 'rect', // pill | rect
            color: 'blue', // gold | blue | silver | black
            tagline: false,
        },
        client: {
            sandbox: "{{$pay['client']['sandbox']}}",
            production: "{{$pay['client']['production']}}",
        },
        payment: function (data, actions) {
            return actions.payment.create({
                payment: {
                    transactions: [{
                        amount: {
                            total: '{{($subTotal-$giftCouponAmount)+$totalTax }}',
                            currency: 'MXN'
                        }
                    }]
                }
            });
        },

        onAuthorize: function (data, actions) {
            jQuery('#place-order-form').submit();
        }}, '#paypal-button-container');
</script>
<script>
    $(function () {

        function calcualateTotal() {
            subTotal = parseFloat(jQuery('.sub-total').attr('data-sub-total')).toFixed(2);
            shippingCost = parseFloat(jQuery('.shipping-cost').attr('data-shipping-cost')).toFixed(2);
            taxAmount = parseFloat(jQuery('.tax-amount').attr('data-tax-amount')).toFixed(2);
            giftCoupon = parseFloat(jQuery('.gif-coupon').attr('data-gif-coupon')).toFixed(2);
            total = ((parseFloat(subTotal) + parseFloat(taxAmount)) - parseFloat(giftCoupon)) + parseFloat(
                shippingCost);
            jQuery('.total').attr('data-total', total.toFixed(2));
            jQuery('.total').html('<strong><span class="price">$' + total.toFixed(2) + '</span>  </strong>');


        }


        /**
         jQu`ry('.tax-calculation').change(function () {
        var data = {
            'name': jQuery(this).attr('data-name'),
            'value': jQuery(this).val(),
            '_token': '{{ csrf_token()  }}'
        };

        $.post({
            data: data,
            type: 'json',
            url: '{{ route("tax.calculation") }}',
            success: function (res) {
                if ((res.success == true)) {
                    jQuery('.tax-amount').html(res.tax_amount_text);
                    jQuery('.tax-amount').attr('data-tax-amount', res.tax_amount);
                    calcualateTotal();
                }
            }
        });
    });
         */
        jQuery('.shipping_option_radio').change(function (e) {

            if (jQuery(this).is(':checked')) {
                var shippingTitle = jQuery(this).attr('data-title');
                var shippingCost = jQuery(this).attr('data-cost');

                jQuery('.shipping-row').removeClass('hidden');

                jQuery('.shipping-row .shipping-title').html(shippingTitle + ":");
                jQuery('.shipping-row .shipping-cost').html("$" + shippingCost);
                jQuery('.shipping-row .shipping-cost').attr('data-shipping-cost', shippingCost);


            } else {
                jQuery('.shipping-row').addClass('hidden');
            }
            calcualateTotal();
        });
        jQuery('#agree').change(function (e) {

            if (jQuery(this).is(':checked')) {

                jQuery('#paypal-button-container').removeAttr('disabled');
                jQuery('#paypal-button-container').removeClass('hidden');

            } else {
                jQuery('#paypal-button-container').attr('disabled');
                jQuery('#paypal-button-container').addClass('hidden');

            }
        });

        jQuery('#place-order-button').click(function (e) {
            jQuery('#place-order-form').submit();
        });
    });
</script> @endpush
@extends('layouts.app') @section('content')
<section class="bannerhead-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-heading">
                    <h1>LISTA DE COMPRAS</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="shoping-cart-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                @if(count($cartProducts)>0)
                <div class="wishlist-table-area table-responsive">
                    <table>
                        <thead>
                            <tr>
                                <th class="product-remove">Eliminar</th>
                                <th class="product-image">Imagen</th>
                                <th class="product-name">Nombre del producto</th>
                                <th class="product-unit-price">Precio unitario</th>
                                <th class="product-quantity">Cantidad</th>
                                <th class="product-edit">Editar</th>
                                <th class="product-subtotal">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; $taxTotal = 0; $giftCouponAmount = 0; ?> @foreach($cartProducts as $product)
                            <?php $total += ($product['final_price'] * $product['qty']) ?>
                            <?php $giftCouponAmount += (isset($product['gift_coupon_amount'])) ? $product['gift_coupon_amount'] : 0.00 ?>
                            <?php $taxTotal += ($product['tax_amount'] * $product['qty']) ?>
                            <form id="{{$product['id']}}" class="" action="{{ route('carrito.eliminar', $product['id'])}}" method="post">
                                <input form="{{$product['id']}}" name="_method" type="hidden" value="PUT">
                                <input form="{{$product['id']}}" name="_token" type="hidden" value="{{csrf_token()}}">
                            </form>
                            <tr>
                                <td class="product-remove">
                                    <a href="{{ route('carrito.eliminar', $product['id'])}}">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                                <td class="product-image">
                                    <a href="{{ route('product.view', $product['slug'])}}">
                                        @if(file_exists( public_path(). $product['relativePath']))
                                        <img alt="{{ $product['name'] }}" src="{{ asset( $product['image']) }}" /> @else
                                        <img alt="{{ $product['name'] }}" src="{{ asset('img/enlace-roto.png')}}" /> @endif
                                    </a>
                                </td>
                                <td class="product-name">
                                    <h3>
                                        <a href="{{ route('product.view', $product['slug'])}}">{{ $product['name'] }}</a>
                                    </h3>
                                </td>

                                <td class="product-unit-price">
                                    <p>$ {{number_format($product['final_price'],2)}}</p>
                                </td>

                                <td class="product-quantity product-cart-details">
                                    <input type="number" value="{{$product['qty']}}" name="qty" form="{{$product['id']}}">
                                </td>
                                <td class="product-edit">
                                    <p>
                                        <a href="javascript:void()" onclick="jQuery('#{{$product['id']}}').submit()">Editar</a>
                                        <p>
                                </td>

                                <td class="product-quantity">
                                    <p>$ {{number_format($product['qty'] * $product['final_price'],2)}}</p>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                @else
                <div class="text-center" style="color:#666666">
                            <h1>Tu carrito está vacío</h1>
                            <h3>¿No sabes qué comprar? ¡Seguro tenemos los zapatos para ti!</h3>
                </div>
                @endif
                <br>
                <br>
                <div class="shopingcart-bottom-area">
                    <div class="bottom-button">
                        <a href="{{route('home')}}" class="bottomb">CONTINUAR COMPRANDO</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if(count($cartProducts)>0)

<section class="discount-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6 res-mr-btm xs-res-mrbtm">
                <div class="discount-main-area disabled">
                    <div class="discount-top">
                        <h3>CODIGO DE DESCUENTO</h3>
                        <p>Ingrese su codigo de descuento si tiene uno</p>
                    </div>
                    <div class="discount-middle">
                        <input type="text" name="code" class="form-control gift-coupon-code-textbox" />
                        <a href="javascript:void(0)" class="code-apply-button">APLICAR</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="subtotal-main-area clearfix">
                    <div class="subtotal-area">
                        <h2>SUBTOTAL
                            <span>${{ number_format($total,2) }}</span>
                        </h2>
                    </div>
                    <div class="subtotal-area">
                        <h2>DESCUENTO
                            <span>$ {{ number_format($giftCouponAmount,2)}}</span>
                        </h2>
                    </div>
                    <div class="subtotal-area">
                        <h2>IVA
                            <span>${{ number_format($taxTotal=($total-$giftCouponAmount)*.16,2) }}</span>
                        </h2>
                    </div>
                    <div class="subtotal-area">
                        <h2>TOTAL A PAGAR
                            <span>$ {{ number_format(($total -$giftCouponAmount)+ $taxTotal,2) }}</span>
                        </h2>
                    </div>
                    <a href="{{ route('checkout.index') }}">PROCEDER </a>
                </div>
            </div>
        </div>
    </div>
</section>
@endempty
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @endsection @push('scripts')
<script>
    jQuery(document).ready(function () {
        jQuery(document).on('click', '.code-apply-button', function (e) {
            e.preventDefault();

            if (jQuery('.gift-coupon-code-textbox').val() == "") {
                alert('please enter valid coupon code');
                return;
            }
            var data = {
                _token: '{{ csrf_token() }}',
                code: jQuery('.gift-coupon-code-textbox').val()
            }

            jQuery.ajax({
                url: "{{ route('get.code_discount') }}",
                dataType: 'json',
                type: 'post',
                data: data,
                success: function (res) {
                    x = res;
                    if (true == res.success) {
                        location.reload();
                    } else {
                        alert(res.message);
                    }
                }
            });
        });
    });
</script> @endpush
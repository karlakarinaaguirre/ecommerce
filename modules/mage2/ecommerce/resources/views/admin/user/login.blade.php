@extends('layouts.app')

@section('content')
<section class="bannerhead-area">
			<div class="container login-area">
				<div class="row">
					<div class="col-md-6">
						<div class="banner-heading">
							<h1>IDENTIFICARSE</h1>
						</div>
					</div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <form method="POST" class="login-form" action="{{ route('admin.login') }}">
              {{ csrf_field() }}

              <h1 class="heading-title">INICIAR SESION</h1>
              <p class="form-row">
                <label>Correo</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

@if ($errors->has('email'))
    <span class="invalid-feedback">
        <strong>{{ $errors->first('email') }}</strong>
    </span>
@endif

              </p>
              <p class="form-row">
                <label>Contraseña</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

@if ($errors->has('password'))
    <span class="invalid-feedback">
        <strong>{{ $errors->first('password') }}</strong>
    </span>
@endif

              </p>
              <p class="lost-password form-group">
                <a  rel="nofollow"class="btn btn-link" href="{{ route('password.reset') }}">
                  Olvido su contraseña?
                </a></p>
              <p class="submit">
                <button class="" name="SubmitLogin" id="submitlogin" type="submit">
                  <span><i class="fa fa-lock"></i>Iniciar sesion</span>
                </button>
              </p>
            </form>
          </div>
				</div>
			</div>
		</section>
@endsection

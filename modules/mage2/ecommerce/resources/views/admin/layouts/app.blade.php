<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'LacaLaca') }}</title>
    <!-- Bootstrap core CSS-->
    <link href="{{ asset('vendor/mage2-admin/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/mage2-admin/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="{{ asset('vendor/mage2-admin/css/sb-admin.css') }}" rel="stylesheet">

    <link href="{{ asset('vendor/mage2-admin/css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.8.1/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" /> @stack('styles')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php
    echo json_encode([
            'csrfToken' => csrf_token(),
    ]);
    ?>
    </script>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">

        <a class="navbar-brand" href="{{route('home')}}">Laca Laca</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
            aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">

            <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
                @if(isset($adminMenus))
                @foreach($adminMenus as $key => $menu)
                @if(count($menu->subMenu()) > 0)
                <?php $subMenu = $menu->subMenu(); ?>

                <?php
                $menu->menuClass();
                ?>

          
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{ $menu->label() }}">
                        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#{{ $menu->label() }}" data-parent="#exampleAccordion">
                            <i class="{{ $menu->icon() }}"></i>
                            <span class="nav-link-text">{{ $menu->label() }}</span>
                        </a>
                        <ul class="sidenav-second-level collapse" id="{{ $menu->label() }}">
                            @foreach($subMenu as $subKey => $subMenuObj)
                            <li>
                                <a href="{{ route($subMenuObj->route()) }}"> {{ $subMenuObj->label() }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>

                    @else

                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{{ $menu->label() }}">
                        <a class="nav-link" href=" {{ route($menu->route()) }}">
                            <i class="{{ $menu->icon() }}"></i>
                            <span class="nav-link-text">{{ $menu->label() }}</span>
                        </a>
                    </li>
                    @endif 
                    @endforeach 
                    @endif

            </ul>
            <ul class="navbar-nav sidenav-toggler">
                <li class="nav-item">
                    <a class="nav-link text-center" id="sidenavToggler">
                        <i class="fa fa-fw fa-angle-left"></i>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.admin-user.show', Auth::guard('admin')->user()->id) }}"><i class="fas fa-user"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                        <i class="fas fa-sign-out"></i>Logout</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="col-12">
                @if(session()->has('notificationText'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Success!</strong> {{ session()->get('notificationText') }}
                </div>
                @endif
            </div>
            {!! Breadcrumb::render(Route::getCurrentRoute()->getName() ) !!} @yield('content')
        </div>

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>




<script src="{{ asset('vendor/mage2-admin/js/app.js') }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.8.1/combined/js/gijgo.min.js" type="text/javascript"></script>
<script src="{{ asset('vendor/mage2-admin/vendor/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/mage2-admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/mage2-admin/vendor/jquery-easing/jquery.easing.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/mage2-admin/js/sb-admin.min.js') }}" type="text/javascript"></script>


<script>
    $(function() {
        $(document).on('click','.has-submenu',function(e) {
            e.preventDefault();
            jQuery(this).parents("li:first").find('.sub-nav:first').toggleClass('d-none');

        });
        $('#toggleNavPosition').click(function () {
        $('body').toggleClass('fixed-nav');
        $('nav').toggleClass('fixed-top static-top');
    });
    });
</script>
@stack('scripts')

</body>

</html>
<?php

namespace Mage2\Ecommerce\Widget\TotalProduct;

use Mage2\Ecommerce\Models\Database\Product;
use Mage2\Ecommerce\Widget\Contracts\Widget as WidgetContract;

class Widget implements WidgetContract
{
    /**
     * Widget View Path.
     *
     * @var string
     */
    protected $view = 'mage2-ecommerce::widget.total-product';

    /**
     * Widget Label.
     *
     * @var string
     */
    protected $label = 'Total Products';

    /**
     * Widget Type.
     *
     * @var string
     */
    protected $type = 'dashboard';

    /**
     * Widget unique identifier.
     *
     * @var string
     */
    protected $identifier = 'total-product';

    public function view()
    {
        return $this->view;
    }

    /*
     * Widget unique identifier
     *
     */
    public function identifier()
    {
        return $this->identifier;
    }

    /*
    * Widget unique identifier
    *
    */
    public function label()
    {
        return $this->label;
    }

    /*
    * Widget unique identifier
    *
    */
    public function type()
    {
        return $this->type;
    }

    /**
     * View Required Parameters.
     *
     * @return array
     */
    public function with()
    {
        $totalProducts = Product::all()->count();

        return ['totalRegisteredProducts' => $totalProducts];
    }
}

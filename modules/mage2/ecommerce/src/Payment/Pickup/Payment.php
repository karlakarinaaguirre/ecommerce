<?php

namespace Mage2\Ecommerce\Payment\Pickup;

use Mage2\Ecommerce\Models\Database\Configuration;
use Mage2\Ecommerce\Payment\Payment as PaymentEcommerce;
use Mage2\Ecommerce\Payment\Contracts\Payment as PaymentContracts;

class Payment extends PaymentEcommerce implements PaymentContracts
{
    /**
     * Payment Option Identifier.
     *
     * @var string
     */
    protected $identifier = 'pickup';

    /**
     * Payment Option Name.
     *
     * @var string
     */
    protected $name = 'Pickup';

    /**
     * Payment Option View.
     *
     * @var string
     */
    protected $view = 'mage2-ecommerce::payment.pickup.index';

    public function isEnabled()
    {
        $isEnabled = Configuration::getConfiguration('mage2_pickup_enabled');
        if (null === $isEnabled || false == $isEnabled) {
            return false;
        }

        return true;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function getName()
    {
        return $this->name;
    }

    public function view()
    {
        return $this->view;
    }

    public function with()
    {
        return [];
    }

    public function process($orderData, $cartProducts = [], $request)
    {
        return true;
    }
}

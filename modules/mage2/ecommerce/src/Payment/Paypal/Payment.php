<?php

namespace Mage2\Ecommerce\Payment\Paypal;

use Mage2\Ecommerce\Models\Database\Configuration;
use Mage2\Ecommerce\Payment\Payment as PaymentEcommerce;
use Mage2\Ecommerce\Payment\Contracts\Payment as PaymentContracts;

class Payment extends PaymentEcommerce implements PaymentContracts
{
    /**
     * Payment Option Identifier.
     *
     * @var string
     */
    protected $identifier = 'paypal';

    /**
     * Payment Option Name.
     *
     * @var string
     */
    protected $name = 'Paypal';
    /**
     * Payment Option View.
     *
     * @var string
     */
    protected $view = 'mage2-ecommerce::payment.paypal.index';

    public function isEnabled()
    {
        $isEnabled = Configuration::getConfiguration('mage2_paypal_enabled');
        if (null === $isEnabled || false == $isEnabled) {
            return false;
        }

        return true;
    }

    protected function getKey()
    {
        return  'mage2_paypal_key';
    }

    protected function getTestKey()
    {
        return  'mage2_paypal_key';
    }

    protected function getSecret()
    {
        return  ($this->getSandbox() !== true) ? 'mage2_paypal_secret_key' : 'mage2_paypal_secret_test_key';
    }

    protected function getSandbox()
    {
        return (Configuration::getConfiguration('mage2_paypal_payment_mode') === 'sandbox') ? true : false;
    }

    protected function isSandbox()
    {
        return Configuration::getConfiguration('mage2_paypal_payment_mode');
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function getName()
    {
        return $this->name;
    }

    public function view()
    {
        return $this->view;
    }

    public function with()
    {
        $token = ($this->getSandbox() !== true) ? Configuration::getConfiguration($this->getKey()) : Configuration::getConfiguration($this->getTestKey());
        $data = ['token' => $token];

        return $data;
    }

    public function getConfig()
    {
        $arr = ['env' => $this->isSandbox(),        'client' => [
            'sandbox' => Configuration::getConfiguration($this->getTestKey()),
            'production' => Configuration::getConfiguration($this->getKey()),
        ]];

        return $arr;
    }

    /*
     * Nothing to do
     *
     */
    public function process($orderData, $cartProducts = [], $request)
    {
    }
}

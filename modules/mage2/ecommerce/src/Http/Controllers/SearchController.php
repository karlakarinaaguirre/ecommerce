<?php

namespace Mage2\Ecommerce\Http\Controllers;

use Illuminate\Http\Request;
use Mage2\Ecommerce\Models\Database\Product;
use Mage2\Ecommerce\Models\Database\Category;

class SearchController extends Controller
{
    public function result(Request $request)
    {
        $queryParam = $request->get('q');

        $products = Product::where('name', 'like', '%'.$queryParam.'%')
            ->where('status', '=', 1)->paginate(9);
        $categoryModel = new Category();

        $categories = $categoryModel->getAllCategories();

        return view('search.results')
        ->with('categories', $categories)
            ->with('queryParam', $queryParam)
            ->with('products', $products);
    }
}

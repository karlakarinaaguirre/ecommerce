<?php

namespace Mage2\Ecommerce\Http\Controllers;

use Mage2\Ecommerce\Models\Database\Configuration;
use Mage2\Ecommerce\Models\Database\Page;
use Mage2\Ecommerce\Models\Database\Product;
use Mage2\Ecommerce\Models\Database\Category;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageModel = null;
        $pageId = Configuration::getConfiguration('general_home_page');

        if (null !== $pageId) {
            $pageModel = Page::find($pageId);
        }

        $category = (Category::where('slug', '=', 'nueva-mercancia')->get()->first() !== null) ? Category::where('slug', '=', 'nueva-mercancia')->get()->first() : Collection::make([]);
        $collections = Product::getCollection()->addCategoryFilter($category->id);
        $nuevaMercancia = $collections->paginateCollection(6);

        return view('home.index')
        ->with('nuevaMercancia', $nuevaMercancia)
        ->with('pageModel', $pageModel);
    }

    public function contact($value = '')
    {
        return view('contact.index');
    }
}

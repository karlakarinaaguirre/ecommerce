<?php

namespace Mage2\Ecommerce\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Mage2\Ecommerce\Payment\Facade as Payment;
use Mage2\Ecommerce\Shipping\Facade as Shipping;
use Mage2\Ecommerce\Models\Database\Country;
use Mage2\Ecommerce\Payment\Paypal\Payment as Paypal;

class CheckoutController extends Controller
{
    public function index()
    {
        $shippingOptions = Shipping::all();
        $paymentOptions = Payment::all();
        $countries = Country::getCountriesOptions();
        $paypal = new Paypal();
        $pay = $paypal->getConfig();

        $cartItems = (Session::get('cart') != null) ? Session::get('cart') : [];
        $view = view('checkout.index')
        ->with('cartItems', $cartItems)
        ->with('countries', $countries)
        ->with('pay', $pay)
        ->with('shippingOptions', $shippingOptions)
        ->with('paymentOptions', $paymentOptions);

        return (count($cartItems) > 0) ? $view : redirect('carrito');
    }
}
